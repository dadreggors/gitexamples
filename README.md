# Git Examples

While these items are meant to help with git, they are mostly only useful on a Unix/Linux based system.
Some of the aliases may be useful on Windows, but the scripts will not help under Windows.
If you are a Unix/Mac/Linux user... these are for you!

## Git scripts

1. git-create-branch
  * This script is used to create a remote tracking branch
  * Great for pushing back up to a branch other than master.
  * Should only be used on a cloned GitLab or GitHub working tree that has an "origin" set.
  * Usage `git-create-branch <branchname>`
1. git-prompt.sh
  * This script allows you to show some great tips in your prompt when in a git working tree:
  * Example: `[ddreggors@loki gitexamples (ddreggors - dev)  ]$`
  * Example usage below (see **Examples**)


**Install:**

Copy these 2 scripts into a bin directory and make them executable.
Now edit your bash profile and update it so that the path to these scripts is in your PATH variable, the git-prompt script is sourced in, and you have exported a prompt.

**Example ()**
```bash
mkdir -p ~/bin
cp -v git* ~/bin
chmod +x ~/bin/git*
```

**Example .bash_profile**
```bash
# Assuming you have copied the git-prompt.sh script into your ~/bin directory
source ~/bin/git-prompt.sh
export PATH="${PATH}:~/bin"
export PROMPT_COMMAND='__git_ps1 "[\[\e[32;1m\]\u\[\e[0m\]@\[\e[32;1m\]\h\[\e[0m\] \W" "\[\e[32;1m\] \[\e[0m\]]$ " " (branch: %s) "'
```

## Git config options


Adding these aliases in the `~/.gitconfig` file will make anyone faster with git. These aliases give very commonly used git commands a handy shortcut to remember. Some of these commands are quite long and the alias makes it dead simple to use and remember.


**Install:**

Copy the git-aliases file somewhere on your filesystem and include them in your `~/.gitconfig` file.
Thats all, have fun using the aliases.

*Example:*
```bash
cp git-aliases ~/.local/share/
echo "
[include]
    path = ~/.local/share/git-aliases" >> ~/.gitconfig
```


**Usage:**

All aliases are meant to be used with the 'git' command prepended to them.


**Examples:**
```bash
git ll
git s
git cam "Commit Message"
git p
git co master
```
